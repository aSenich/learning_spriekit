//
//  Coin.swift
//  Pierre The Penguin
//
//  Created by Austin Senich on 2/20/19.
//  Copyright © 2019 Austin Senich. All rights reserved.
//

import SpriteKit

class Coin: SKSpriteNode, GameSprite {

    
    var initalSize: CGSize = CGSize(width: 50, height: 44)
    var textureAtlas: SKTextureAtlas = SKTextureAtlas(named: "Coins")
    var value = 3
    
    init() {
        let bronzeCoinTexture = textureAtlas.textureNamed("3Coin")
        super.init(texture: bronzeCoinTexture, color: .clear, size: initalSize)
        
        self.physicsBody = SKPhysicsBody(circleOfRadius: size.width / 2)
        self.physicsBody?.affectedByGravity = false
        
        //         temp
//        self.physicsBody?.isDynamic = false
        self.physicsBody?.categoryBitMask = PhysicsCategory.coin.rawValue
        self.physicsBody?.collisionBitMask = 0
    }
    

    func turnCoinGold() {
        self.texture = textureAtlas.textureNamed("5Coin")
        self.value = 5
    }
    
    func ontap() {
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func collect() {
        let lightImpact = UIImpactFeedbackGenerator(style: .light)
        lightImpact.prepare()
//        prevent futher contact
        self.physicsBody?.categoryBitMask = 0
//         fade out, move up, and scale up the coin
        let collectAnimation = SKAction.group([SKAction.fadeAlpha(to: 0, duration: 0.2), SKAction.scale(to: 1.5, duration: 0.2), SKAction.move(by: CGVector(dx: 0, dy: 25), duration: 0.2)])
//        after fading out move coin out of the way and reset it to be placed again when the level respawns
        let resetAfterCollection = SKAction.run {
            self.position.y = 5000
            self.alpha = 1
            self.xScale = 1
            self.yScale = 1
            self.physicsBody?.categoryBitMask = PhysicsCategory.coin.rawValue
        }
        
//        combine actions into sequence
        let collectSequence = SKAction.sequence([collectAnimation, resetAfterCollection])
        lightImpact.impactOccurred()
//        run combine animation
        self.run(collectSequence)
    }
    
    
}
