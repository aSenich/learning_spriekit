//
//  Clouds.swift
//  Pierre The Penguin
//
//  Created by Austin Senich on 1/6/19.
//  Copyright © 2019 Austin Senich. All rights reserved.
//

import SpriteKit

class Cloud: SKSpriteNode, GameSprite {

    
    var initalSize: CGSize = CGSize(width: 60, height: 60)
    var textureAtlas: SKTextureAtlas = SKTextureAtlas(named: "Sprites")
    var movingAnimation = SKAction()
    
     init() {
        super.init(texture: nil, color: .clear, size: initalSize)
        
        createFlyAnimation()
        self.run(movingAnimation)
        self.physicsBody = SKPhysicsBody(circleOfRadius: size.width / 2)
        self.physicsBody?.affectedByGravity = false
//         temp
        self.physicsBody?.isDynamic = false
        self.physicsBody?.categoryBitMask = PhysicsCategory.cloud.rawValue
        self.physicsBody?.collisionBitMask = ~PhysicsCategory.damagedSun.rawValue
    }
    
    func createFlyAnimation() {
        let movingFrames: [SKTexture] = [textureAtlas.textureNamed("Cloud"), textureAtlas.textureNamed("MovingCloud")]
        let movingAction = SKAction.animate(with: movingFrames, timePerFrame: 0.15)
        
        movingAnimation = SKAction.repeatForever(movingAction)
    }
    
    func ontap() {
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
