//
//  Ground.swift
//  Pierre The Penguin
//
//  Created by Austin Senich on 1/6/19.
//  Copyright © 2019 Austin Senich. All rights reserved.
//

import SpriteKit

class Ground: SKSpriteNode, GameSprite {
    
    var jumpWidth = CGFloat()
    
    var jumpCount = CGFloat(1)

    
    var textureAtlas: SKTextureAtlas = SKTextureAtlas(named: "Environment")
    
    var initalSize: CGSize = CGSize.zero
    
    func createChildNode() {
        self.anchorPoint = CGPoint(x: 0, y: 1)
        
        let texture = textureAtlas.textureNamed("Ground")
        
        var titleCount: CGFloat = 0
        
        let tileSize = CGSize(width: 35, height: 300)
        
        while titleCount * tileSize.width < self.size.width {
            let tileNode = SKSpriteNode(texture: texture)
            tileNode.size = tileSize
            tileNode.position.x = titleCount * tileSize.width
            
            tileNode.anchorPoint = CGPoint(x: 0, y: 1)
            tileNode.zPosition = 1
            
            self.addChild(tileNode)
            
            titleCount += 1
        }
        

        let pointTopRight = CGPoint(x: 0, y: 0 )
        let pointTopLeft = CGPoint(x: size.width, y: 0)
        self.physicsBody = SKPhysicsBody(edgeFrom: pointTopLeft, to: pointTopRight)
        self.physicsBody?.categoryBitMask = PhysicsCategory.ground.rawValue
        
        jumpWidth = tileSize.width * floor(titleCount / 3)
        
        
    }
    
    func checkForReposition(playerProgress: CGFloat) {
        let groundJumpPosition = jumpWidth * jumpCount
        
        if playerProgress >= groundJumpPosition {
            self.position.x += jumpWidth
            jumpCount += 1
        }
    }
    
    func ontap () {
        
    }
}
