//
//  GameScene.swift
//  Pierre The Penguin
//
//  Created by Austin Senich on 1/5/19.
//  Copyright © 2019 Austin Senich. All rights reserved.
//

import SpriteKit
//import GameplayKit
import CoreMotion


enum PhysicsCategory: UInt32 {
    case sun = 1
    case damagedSun = 2
    case ground = 4
    case enemy = 8
    case coin = 16
    case powerup = 32
    case cloud = 64
    case house = 128
}


class GameScene: SKScene, SKPhysicsContactDelegate {

    
    let player = Player()
    
    let powerUp = PowerUp()
    
    let cam = SKCameraNode()
    
    let ground = Ground()

    let motionManager = CMMotionManager()
    
    var screenCenterY: CGFloat = 0
    
    let initalPlayerPosition = CGPoint(x: 150, y: 250)
    var playerProgress = CGFloat()
    
    let encounterManager = EncounterManger()
    
    var nextEncounterSpawnPosition = CGFloat(150)
    
//    keeps track of how many coins have been collected
    var coinsCollected = 0
    
//    hud instance
    let hud = HUD()
    
    override func didMove(to view: SKView) {
//        Calculate center of the screen
        screenCenterY = self.size.height / 2
//        self.backgroundColor = UIColor(red: 0.4, green: 0.6, blue: 0.95, alpha: 1)
        
        self.anchorPoint = .zero
        
        self.camera = cam
        
       
        
//        self.addBG()
        
        ground.position = CGPoint(x: -self.size.width * 2, y: 5)
        
        ground.size = CGSize(width: self.size.width * 6, height: 0)
        
        ground.createChildNode()
        
        self.addChild(ground)
        
        player.position = initalPlayerPosition
        player.zPosition = 1
        self.addChild(player)
        
        


        //        MARK: Core Motion
        self.motionManager.startAccelerometerUpdates()
        
        encounterManager.addEncountersToScene(gameScene: self)
//        encounterManager.encounters[0].position = CGPoint(x: 400, y: 330)
        
//        render in powerUp
        self.addChild(powerUp)
        powerUp.position = CGPoint(x: -2000, y: -2000)
        
        self.physicsWorld.contactDelegate = self
        
//        add camera itself to node tree
        self.addChild(self.camera!)
        self.camera!.zPosition = 50
        
        hud.createHudNodes(screensize: self.size)
        
//        add HUD to camera's node tree
        self.camera!.addChild(hud)
        
 

        startDotEmitter()
        
    }
    
    override func didSimulatePhysics() {
//        Tracks progress
        playerProgress = player.position.x - initalPlayerPosition.x
//        Keep camera locked at center y by defult
        var cameraYPos = screenCenterY
        cam.yScale = 1
        cam.xScale = 1
//        Follow player if they go above midway
        if (player.position.y > screenCenterY) {
            cameraYPos = player.position.y
//            scale out as camera goes up
            let percentageOfMaxHeight = (player.position.y - screenCenterY) / (player.maxHeight - screenCenterY)
            let newScale = 1 + percentageOfMaxHeight
            cam.yScale = newScale
            cam.xScale = newScale
        }
        self.camera!.position = CGPoint(x: player.position.x, y: cameraYPos)
        
        ground.checkForReposition(playerProgress: playerProgress)
        
        if player.position.x > nextEncounterSpawnPosition {
            encounterManager.placeNextEncounter(currentXPos: nextEncounterSpawnPosition)
            nextEncounterSpawnPosition += 1000
            player.forwardVelocity += 10
            player.numberofspeedUps += 1
            
//            Make there be a 10% chance of spawning a powerup
//            let powerupRoll = Int(arc4random_uniform(10))
            let powerupRoll = 0
            if powerupRoll == 0 {
//                only move powerup if its off the screen
                if abs(player.position.x - powerUp.position.x) > 1200 {
// select random y position from 50 to 450
                    let randomYPos = 50 + CGFloat(arc4random_uniform(400))
                    powerUp.position = CGPoint(x: nextEncounterSpawnPosition, y: randomYPos)
//                     remove any previous velocity
                    powerUp.physicsBody?.angularVelocity = 0
                    powerUp.physicsBody?.velocity = CGVector(dx: 0, dy: 0)
                }
            }
        }
//        print(player.position)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
//        Get touch location
        for touch in (touches) {
//            Get touch location
            let location = touch.location(in: self)
//            Locate node at this position
            let nodeTouched = atPoint(location)
//            Atempt to hand off to sprite protocole
            if let gamesprite = nodeTouched as? GameSprite {
//                if node follows protocol
                gamesprite.ontap()
            }
        }
        player.touchingStarted()
    }
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        player.touchingStoped()
    }
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        player.touchingStoped()
    }
    
    
    func addBG() {
        let bg = SKSpriteNode(imageNamed: "Background")
//        bg.position = CGPoint(x: 175, y: 250)
        bg.anchorPoint = .zero
        bg.position = .zero
        bg.zPosition = 0
        


        self.addChild(bg)


    }
    
  
    override func update(_ currentTime: TimeInterval) {
        player.update()
        
        /*
//        Core Motion Code
        if let accelData = self.motionManager.accelerometerData {
            var forceAmount: CGFloat
            var movement = CGVector()
            
            switch UIApplication.shared.statusBarOrientation {
            case .landscapeLeft:
                forceAmount = 20000
            case .landscapeRight:
                forceAmount = -20000
            default:
                forceAmount = 0
            }
            
            if accelData.acceleration.y > 0.15 {
                movement.dx = forceAmount
            } else if accelData.acceleration.y < -0.15 {
                movement.dx = -forceAmount
            }
            
            player.physicsBody?.applyForce(movement)
 
            
        }
        */
        
    }
    
    func didBegin(_ contact: SKPhysicsContact) {
        let otherBody: SKPhysicsBody
        
        let sunMask = PhysicsCategory.sun.rawValue | PhysicsCategory.damagedSun.rawValue
        
        if (contact.bodyA.categoryBitMask & sunMask) > 0 {
            otherBody = contact.bodyB
        } else {
            otherBody = contact.bodyA
        }
        
//        find type of contact
        switch otherBody.categoryBitMask {
        case PhysicsCategory.ground.rawValue:
            player.takeDamage()
            hud.setHealthDisplay(newHealth: player.health)
        case PhysicsCategory.enemy.rawValue:
            player.takeDamage()
            hud.setHealthDisplay(newHealth: player.health)
        case PhysicsCategory.coin.rawValue:
            if let coin = otherBody.node as? Coin {
//                 call collect animation
                coin.collect()
//                add 1 to coin counter
                self.coinsCollected += coin.value
                hud.setCoinCountDisplay(newCoinCount: self.coinsCollected)
//                print("Coin collected with value = \(coin.value)")
            }
        case PhysicsCategory.powerup.rawValue:
            let turnOnPowerUpFire = SKAction.run {
                self.powerUpEmitter(on: true)
            }
            let turnOffPowerUpFire = SKAction.run {
                self.powerUpEmitter(on: false)
            }
            let fireSequence = SKAction.sequence([turnOnPowerUpFire, SKAction.wait(forDuration: 9.5), turnOffPowerUpFire])
            player.powerUp()
            self.run(fireSequence)
        case PhysicsCategory.house.rawValue:
            player.takeDamage()
            hud.setHealthDisplay(newHealth: player.health)
        case PhysicsCategory.cloud.rawValue:
            player.takeDamage()
            hud.setHealthDisplay(newHealth: player.health)
        default:
//            print("Player hit an object with no game logic")
            break
        }
    }
 

    func powerUpEmitter(on: Bool) {
        if let fireEmitter = SKEmitterNode(fileNamed: "TestFire") {
            if on == true {
            fireEmitter.particleZPosition = 1
                player.removeAllChildren()
            player.addChild(fireEmitter)
            fireEmitter.targetNode = self
            } else {
                player.removeAllChildren()
                startDotEmitter()
            }
        }
    }
    
    func startDotEmitter() {
        //        dot emitter
        if let dotEmitter = SKEmitterNode(fileNamed: "Path") {
            //            position pplayer infront of other game objects
            player.zPosition = 10
            //            place dotemitter behind player
            dotEmitter.particleZPosition = -1
            //            add partical emitter to player
            player.addChild(dotEmitter)
            
            dotEmitter.targetNode = self
        }
    }
    
}
