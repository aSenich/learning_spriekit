//
//  PowerUp.swift
//  Pierre The Penguin
//
//  Created by Mark Senich on 2/18/19.
//  Copyright © 2019 Austin Senich. All rights reserved.
//

import UIKit
import SpriteKit

class PowerUp: SKSpriteNode, GameSprite {
    func ontap() {
        
    }
    
    var initalSize = CGSize(width: 45, height: 45)
    var textureAtlas: SKTextureAtlas = SKTextureAtlas(named: "PowerUp")
    var rotatingAnimation = SKAction()
    
    init() {
        super.init(texture: nil, color: .clear, size: initalSize)
        
        createAnimations()
        self.run(rotatingAnimation)
        
        self.physicsBody = SKPhysicsBody(circleOfRadius: size.width / 2)
        self.physicsBody?.allowsRotation = false
        
        //         temp
        self.physicsBody?.isDynamic = false
        
        self.physicsBody?.categoryBitMask = PhysicsCategory.powerup.rawValue
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func createAnimations() {
        let movingFrames: [SKTexture] = [textureAtlas.textureNamed("PUFrame1"), textureAtlas.textureNamed("PUFrame2"), textureAtlas.textureNamed("PUFrame3"), textureAtlas.textureNamed("PUFrame4"), textureAtlas.textureNamed("PUFrame5"), textureAtlas.textureNamed("PUFrame6"), textureAtlas.textureNamed("PUFrame7"), textureAtlas.textureNamed("PUFrame8")]
        let motionAction = SKAction.animate(with: movingFrames, timePerFrame: 0.1)
        
        rotatingAnimation = SKAction.repeatForever(motionAction)
        
    }
}
