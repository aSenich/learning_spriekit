//
//  GameSprite.swift
//  Pierre The Penguin
//
//  Created by Austin Senich on 1/6/19.
//  Copyright © 2019 Austin Senich. All rights reserved.
//

import SpriteKit

protocol GameSprite {
    var textureAtlas: SKTextureAtlas {get set}
    var initalSize: CGSize {get set}
    func ontap()
}
