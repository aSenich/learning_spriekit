//
//  GameViewController.swift
//  Pierre The Penguin
//
//  Created by Austin Senich on 1/5/19.
//  Copyright © 2019 Austin Senich. All rights reserved.
//

import UIKit
import SpriteKit
import GameplayKit

class GameViewController: UIViewController {

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        if let view = self.view as! SKView? {
//            Load the SKScene from "GameScene.sks
            if let scene = SKScene(fileNamed: "GameScene") {
                //                Set the cale mode to fit the window:
                scene.scaleMode = .aspectFill
                
                //                sizes the scene to fit the view exactly:
                scene.size = view.bounds.size
//                Shows new scene
                view.presentScene(scene)
            }
            
            view.ignoresSiblingOrder = true
            view.showsFPS = true
            view.showsNodeCount = true
            
        }
    }

    override var shouldAutorotate: Bool {
        return true
    }

    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .landscape
    }

    override var prefersStatusBarHidden: Bool {
        return true
    }
}
