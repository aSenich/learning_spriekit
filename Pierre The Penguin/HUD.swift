//
//  HUD.swift
//  Pierre The Penguin
//
//  Created by Mark Senich on 2/23/19.
//  Copyright © 2019 Austin Senich. All rights reserved.
//

import SpriteKit

class HUD: SKNode {
    var textureAtlas = SKTextureAtlas(named: "HUD")
    var coinAtlas = SKTextureAtlas(named: "Coins")
//    array to keep track of hearts
    var heartNodes: [SKSpriteNode] = []
//    SKLabelNode to print coin score
    var coinCountText = SKLabelNode(text: "000000")
    
    
    func createHudNodes(screensize: CGSize) {
        let cameraOrigin = CGPoint(x: screensize.width / 2, y: screensize.height / 2)
        
//        create coin counter
        let coinIcon = SKSpriteNode(texture: coinAtlas.textureNamed("3Coin"))
//        size and position coin icon
        let coinPostion = CGPoint(x: -cameraOrigin.x + 23
            , y: cameraOrigin.y - 23)
        coinIcon.size = CGSize(width: 25, height: 22)
        coinIcon.position = coinPostion
//        configure coin text
        coinCountText.fontName = "AvenirNext-HeavyItalic"
        let coinTextPosition = CGPoint(x: -cameraOrigin.x + 41, y: coinPostion.y)
        coinCountText.position = coinTextPosition
//         align text relative to SKLabelNode position
        coinCountText.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.left
        coinCountText.verticalAlignmentMode = SKLabelVerticalAlignmentMode.center
        self.addChild(coinCountText)
        self.addChild(coinIcon)
        
//        create three heart nodes for the life meter
        for index in 0..<3 {
            let newHeartNode = SKSpriteNode(texture: textureAtlas.textureNamed("Heart"))
            newHeartNode.size = CGSize(width: 46, height: 40)
//            position hearts below coins
            let xPos = -cameraOrigin.x + CGFloat(index * 58) + 33
            let yPos = cameraOrigin.y - 66
            newHeartNode.position = CGPoint(x: xPos, y: yPos)
//            keep track of heart nodes in array
            heartNodes.append(newHeartNode)
//            add heart to HUD
            self.addChild(newHeartNode)
        }
    }
    
    func setCoinCountDisplay(newCoinCount: Int) {
        //        use NSNumberFormatter class to set the 0's in the player's score
        let formatter = NumberFormatter()
        let number = NSNumber(value: newCoinCount)
        formatter.minimumIntegerDigits = 6
        if let coinStr = formatter.string(from: number) {
            //            update leabel node
            coinCountText.text = coinStr
        }
    }
    
    func setHealthDisplay(newHealth: Int) {
//        create a fade SKAction to dae out lost hearts
        let fadeAction = SKAction.fadeAlpha(to: 0.2, duration: 0.3)
//        loop through each heart and update staus
        for index in 0 ..< heartNodes.count {
            if index < newHealth {
                heartNodes[index].alpha = 1
            } else {
//                fade heart
                heartNodes[index].run(fadeAction)
            }
        }
    }
}
