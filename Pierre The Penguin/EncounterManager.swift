//
//  EncounterManager.swift
//  Pierre The Penguin
//
//  Created by Austin Senich on 2/20/19.
//  Copyright © 2019 Austin Senich. All rights reserved.
//

import SpriteKit

class EncounterManger {
// Store encounter file names
    let encounterNames: [String] = ["EncounterA", "EncounterB", "EncounterC", "EncounterD"]
//    add encounter node
    var encounters: [SKNode] = []
    //    MARK: Tracks encounters
    var currentEncounterIndex: Int?
    var previousEncounterIndex: Int?
    
    init() {
//loop through each encounter
        for encounterFileNAme in encounterNames {
//            create node for encounter
            let encounterNode = SKNode()
            //            Load scene file into SKSene instance:
            if let encounterScene = SKScene(fileNamed: encounterFileNAme) {
//                loop through each child node in the SKScene
                for child in encounterScene.children {
//                    copy each scene's child nodes and add it to encounter node
                    let copyOfNode = type(of: child).init()
//                    save node's position to copy
                    copyOfNode.position = child.position
//                    save node's name to copy
                    copyOfNode.name = child.name
//                    add node copy to encounter node
                    encounterNode.addChild(copyOfNode)
                }
            }
            encounters.append(encounterNode)
            saveSpriteInitalPositions(node: encounterNode)
            
            encounterNode.enumerateChildNodes(withName: "gold") { (node: SKNode, UnsafeMutablePointer) in
                (node as? Coin)?.turnCoinGold()
            }
        }
        
    }
    
    func addEncountersToScene(gameScene: SKNode) {
        var encounterPosY = 1000
        for encounterNode in encounters {
//            spawn encounters behind action
            encounterNode.position = CGPoint(x: -2000, y: encounterPosY)
            gameScene.addChild(encounterNode)
//            double y pos for next spawn
            encounterPosY *= 2
        }
    }
    
    func saveSpriteInitalPositions(node: SKNode) {
        for sprite in node.children {
            if let spriteNode = sprite as? SKSpriteNode {
                let initialPositionValue = NSValue.init(cgPoint: sprite.position)
                spriteNode.userData = ["initialPosition" : initialPositionValue]
                saveSpriteInitalPositions(node: spriteNode)
            }
        }
    }
    
    func resetSpritePositions(node: SKNode) {
        for sprite in node.children {
            if let spriteNode = sprite as? SKSpriteNode {
                spriteNode.physicsBody?.velocity = CGVector(dx: 0, dy: 0)
                spriteNode.physicsBody?.angularVelocity = 0
//                Reset position
                spriteNode.zRotation = 0
                if let initialPositionValue = spriteNode.userData?.value(forKey: "initialPosition") as? NSValue {
                    spriteNode.position = initialPositionValue.cgPointValue
                }
                resetSpritePositions(node: spriteNode)
            }
        }
    }
    
    func placeNextEncounter(currentXPos: CGFloat) {
        let encounterCount = UInt32(encounters.count)
        print(encounterCount)
        if encounterCount < 3 {
            return
        }
        
        var nextEncounterIndex: Int?
        var trulyNew: Bool?
        
        while trulyNew == false || trulyNew == nil {
            nextEncounterIndex = Int(arc4random_uniform(encounterCount))
            
            trulyNew = true
            
            if let currentIndex = currentEncounterIndex {
                if (nextEncounterIndex == currentIndex) {
                    trulyNew = false
                }
            }
        }
        
        previousEncounterIndex = currentEncounterIndex
        currentEncounterIndex = nextEncounterIndex!
        
//        reset and place new encounter
        print("Current encounter index: \(currentEncounterIndex)")
        let encounter = encounters[currentEncounterIndex!]
        encounter.position = CGPoint(x: currentXPos + 1000, y: 330)
        resetSpritePositions(node: encounter)
        
    }
}
