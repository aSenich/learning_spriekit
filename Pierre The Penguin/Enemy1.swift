//
//  Enemy1.swift
//  Pierre The Penguin
//
//  Created by Austin Senich on 2/18/19.
//  Copyright © 2019 Austin Senich. All rights reserved.
//

import SpriteKit

class Enemy1: SKSpriteNode, GameSprite {
    func ontap() {
        
    }
    
    var initalSize = CGSize(width: 48, height: 58)
    var textureAtlas: SKTextureAtlas = SKTextureAtlas(named: "Sprites")
    var animation = SKAction()
    
    init() {
        let enemy = textureAtlas.textureNamed("Enemy")
        super.init(texture: enemy, color: .clear, size: initalSize)
        self.physicsBody = SKPhysicsBody(rectangleOf: initalSize)
        self.physicsBody?.affectedByGravity = false
        
        //         temp
        self.physicsBody?.isDynamic = false
        self.physicsBody?.categoryBitMask = PhysicsCategory.enemy.rawValue
        self.physicsBody?.collisionBitMask = ~PhysicsCategory.damagedSun.rawValue
        
        createAnimation()
        self.run(animation)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func createAnimation() {
        let pulseInGroup = SKAction.group([SKAction.fadeAlpha(to: 0.85, duration: 0.15), SKAction.scale(to: 0.85, duration: 0.8)])
        let pulseOutGroup = SKAction.group([SKAction.fadeAlpha(to: 1.0, duration: 0.15), SKAction.scale(to: 1, duration: 0.8)])
        
        let pulseSequence = SKAction.sequence([pulseInGroup, pulseOutGroup])
        animation = SKAction.repeatForever(pulseSequence)
    }
}
