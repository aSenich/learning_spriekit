//
//  Player.swift
//  Pierre The Penguin
//
//  Created by Austin Senich on 1/13/19.
//  Copyright © 2019 Austin Senich. All rights reserved.
//
import UIKit
import SpriteKit

class Player: SKSpriteNode, GameSprite {
    
    var tappingScreen = false
//    Set maximum upwards force
    let maxFlappingForce: CGFloat = 75000
//    max height
    let maxHeight: CGFloat = 1000

    var numberofspeedUps = 0
    var forwardVelocity: CGFloat = 200
    
    var initalSize = CGSize(width: 50, height: 50)

    var textureAtlas: SKTextureAtlas = SKTextureAtlas(named: "Sun")
    
    var motionAnimation = SKAction()
    
//    Handles health
    var health: Int = 3
//    keeps track of when player is invlnerable
    var invulnerable = false
//    tracks when player is newly damaged
    var damaged = false
//    damaged animaton
    var damagedAnimation = SKAction()
//    die animation
    var dieAnimaton = SKAction()
    
    init() {
        let bodyTexture = textureAtlas.textureNamed("tempCharacter")
        super.init(texture: bodyTexture, color: .clear, size: initalSize)
        createAnimation()
//        self.run(motionAnimation)
        
//        create physics body
//        let bodyTexture = textureAtlas.textureNamed("Sun1")
        
        
        self.physicsBody = SKPhysicsBody(circleOfRadius: size.width / 2)
//        lose momentum
        self.physicsBody?.linearDamping = 0.9
//        Add weight
        self.physicsBody?.mass = 30
//        stop from rotating
        self.physicsBody?.allowsRotation = false
        
        self.physicsBody?.categoryBitMask = PhysicsCategory.sun.rawValue
        self.physicsBody?.contactTestBitMask = PhysicsCategory.enemy.rawValue | PhysicsCategory.ground.rawValue | PhysicsCategory.powerup.rawValue | PhysicsCategory.coin.rawValue | PhysicsCategory.cloud.rawValue | PhysicsCategory.house.rawValue
        self.physicsBody?.collisionBitMask = PhysicsCategory.ground.rawValue
        
//        start
        self.physicsBody?.affectedByGravity = false
//        give player time before gravity starts
        let startGravitysequence = SKAction.sequence([SKAction.wait(forDuration: 1.0), SKAction.run {
            self.physicsBody?.velocity.dy = 80
            self.physicsBody?.affectedByGravity = true
            }])
        
        self.run(startGravitysequence)
    }
    
    func createAnimation() {
//        let sunMovingFrames: [SKTexture] = [textureAtlas.textureNamed("Sun1"), textureAtlas.textureNamed("Sun2"), textureAtlas.textureNamed("Sun3")]
        
//        let motion = SKAction.animate(with: sunMovingFrames, timePerFrame: 0.35)
        
//        motionAnimation = SKAction.repeatForever(motion)
        
        let damagedStart = SKAction.run {
    //            allows player to pass through enemies
            self.physicsBody?.categoryBitMask = PhysicsCategory.damagedSun.rawValue
        }
//        create opacity pulse, starts off slow then speeds up at the end
        let slowFade = SKAction.sequence([SKAction.fadeAlpha(to: 0.3, duration: 0.35), SKAction.fadeAlpha(to: 0.7, duration: 0.35)])
        let fastFade = SKAction.sequence([SKAction.fadeAlpha(to: 0.3, duration: 0.2), SKAction.fadeAlpha(to: 0.7, duration: 0.2)])
        
        let fadeInandOut = SKAction.sequence([SKAction.repeat(slowFade, count: 2), SKAction.repeat(fastFade, count: 5), SKAction.fadeAlpha(to: 1, duration: 0.15)])
        
//        return sun to normal contact state
        let damageEnd = SKAction.run {
            self.physicsBody?.categoryBitMask = PhysicsCategory.sun.rawValue
//            turn off damaged indicator
            self.damaged = false
        }
        
//        store all damaged things into damagedAnimation
        self.damagedAnimation = SKAction.sequence([damagedStart, fadeInandOut, damageEnd])
        
        
//        Die aniamtions
        
        
        let startDie = SKAction.run {
//            susspend in air
            self.physicsBody?.affectedByGravity = false
//            stop any motion
            self.physicsBody?.velocity = CGVector(dx: 0, dy: 0)
        }
        
        let endDie = SKAction.run {
//            turn gravity back on
            self.physicsBody?.affectedByGravity = true
        }
        
        self.dieAnimaton = SKAction.sequence([
            startDie,
// scale player
            SKAction.scale(to: 1.5, duration: 0.5),
//            pause
            SKAction.wait(forDuration: 0.5),
//            end die (turn gravity back on
            endDie
            ])
    }
    
    func ontap() {

    }
    func update() {
        
        var forceToApply = maxFlappingForce
        
        if self.tappingScreen {
        
        if position.y > 600 {
            let percentageOfMaxHeight = position.y / maxHeight
            let flappingForceSubtraction = percentageOfMaxHeight * maxFlappingForce
            forceToApply -= flappingForceSubtraction
        }
//        Apply final force
        self.physicsBody?.applyForce(CGVector(dx: 0, dy: forceToApply))
//        Limit velocity
        if self.physicsBody!.velocity.dy > 300 {
            self.physicsBody!.velocity.dy = 300
        }
        }

//        moves player to the right
        print(forwardVelocity)
        self.physicsBody?.velocity.dx = self.forwardVelocity
        
    }
    
    func touchingStarted() {
        if self.health <= 0 {
            return
        }
        self.tappingScreen = true
    }
    func touchingStoped() {
        if self.health <= 0 {
            return
        }
        self.tappingScreen = false
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func die() {
//        make sure palyer is fully visible
        self.alpha = 1
//        remove all animations
        self.removeAllActions()
//        run die andimation
        self.run(dieAnimaton)
//        prevent any futher flying upward
        self.tappingScreen = false
//        stop forward movement
        self.forwardVelocity  = 0
    }
    
    func takeDamage() {
        let heavyImpact = UIImpactFeedbackGenerator(style: .heavy)
        heavyImpact.prepare()
//        If player is damaged or invalnerable reture function
        if self.invulnerable || self.damaged {
            return
        }
//        sets sun's state to damaged
        self.damaged = true
//        remove one form health pool
        self.health -= 1
        if self.health == 0 {
            heavyImpact.impactOccurred()
//            if health = 0 then execute die function
            die()
        } else {
            heavyImpact.impactOccurred()
//            run damage animation
            self.run(damagedAnimation)
            
        }
    }
    
    func powerUp() {
//        remove any existing power-up animations
        self.removeAction(forKey: "powerup")
//        make player move twice as fast
        self.forwardVelocity = forwardVelocity + 100
//        make player invalnerable
        self.invulnerable = true
//         create sequence that will make player big for 8 seconds
        let powerupSequence = SKAction.sequence([SKAction.scale(to: 1.5, duration: 0.3), SKAction.wait(forDuration: 8), SKAction.scale(to: 1, duration: 1), SKAction.run {
            self.forwardVelocity = CGFloat(200 + 10 * self.numberofspeedUps)
            self.invulnerable = false
            }])
        self.run(powerupSequence, withKey: "powerup")
    }
}


