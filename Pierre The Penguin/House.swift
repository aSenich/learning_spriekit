//
//  House.swift
//  Pierre The Penguin
//
//  Created by Austin Senich on 2/20/19.
//  Copyright © 2019 Austin Senich. All rights reserved.
//

import SpriteKit

class House: SKSpriteNode, GameSprite {

    var initalSize: CGSize = CGSize(width: 75, height: 97)
    var textureAtlas: SKTextureAtlas = SKTextureAtlas(named: "Environment")
    
    init() {
        super.init(texture: textureAtlas.textureNamed("House"), color: .clear, size: initalSize)
        
        self.physicsBody = SKPhysicsBody(texture: textureAtlas.textureNamed("House"), size: initalSize)
        self.physicsBody?.affectedByGravity = false
        self.physicsBody?.isDynamic = false
        self.physicsBody?.categoryBitMask = PhysicsCategory.house.rawValue
        self.physicsBody?.collisionBitMask = ~PhysicsCategory.damagedSun.rawValue
    }
    
    func ontap() {
        
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
